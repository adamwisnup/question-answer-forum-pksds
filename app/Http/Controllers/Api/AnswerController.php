<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Answer;
use App\Models\Question;
use Illuminate\Http\Request;

class AnswerController extends Controller
{
    public function store(Request $request, $questionId)
    {
        $request->validate([
            'user_id' => 'required|exists:users,id',
            'content' => 'required|string',
        ]);

        $question = Question::findOrFail($questionId);

        $answer = Answer::create([
            'user_id' => $request->input('user_id'),
            'question_id' => $question->id,
            'content' => $request->input('content'),
        ]);

        return response()->json([
            'code' => 200,
            'message' => 'Created',
            'data' => $answer
        ], 201);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'content' => 'required|string',
        ]);

        $answer = Answer::findOrFail($id);

        if (!$answer) {
            return response()->json([
                'code' => 404,
                'message' => 'Answer not found'
            ], 404);
        }

        $answer->update($request->all());
        return response()->json([
            'code' => 200,
            'message' => 'OK',
            'data' => $answer
        ]);
    }

    public function destroy($id)
    {
        $answer = Answer::findOrFail($id);

        if (!$answer) {
            return response()->json([
                'code' => 404,
                'message' => 'Answer not found'
            ], 404);
        }

        $answer->delete();
        return response()->json([
            'code' => 200,
            'message' => 'Answer deleted successfully'
        ]);
    }
}
