<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Auth;

class AnswerController extends Controller
{

    public function store(Request $request, $questionId)
    {
        $request->validate([
            'content' => 'required',
        ]);

        $iduser = Auth::id();

        $question = Question::findOrFail($questionId);
        $answer = new Answer();

        $answer->user_id = $iduser;
        $answer->content = $request->content;
        // $answer->question_id = $request->content;
        $answer->question_id = $questionId;


        // $question->answers()->save($answer);
        $answer->save();

        return redirect('/questions/' . $questionId)->with('success', 'Answer created successfully.');
    }

    public function edit($questionId, $answerId)
    {
        $question = Question::findOrFail($questionId);
        $answer = $question->answers()->findOrFail($answerId);

        return view('answers.edit', compact('answer'));
    }

    public function update(Request $request, $questionId, $answerId)
    {
        $request->validate([
            'content' => 'required',
        ]);

        $question = Question::findOrFail($questionId);
        $answer = $question->answers()->findOrFail($answerId);
        $answer->content = $request->content;
        $answer->save();

        return redirect('/questions/' . $questionId)->with('success', 'Answer updated successfully.');
    }

    public function destroy($questionId, $answerId)
    {
        $question = Question::findOrFail($questionId);
        $answer = $question->answers()->findOrFail($answerId);
        $answer->delete();

        return redirect('/questions/' . $questionId)->with('success', 'Answer deleted successfully.');
    }
}
