<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Category;
use App\Models\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Storage;

class QuestionController extends Controller
{
    public function index()
    {
        $questions = Question::all();

        return view('questions.index', compact('questions'));
    }

    public function show($id)
    {
        $question = Question::findOrFail($id);
        $answers = Answer::where('question_id', $id)->get();

        return view('questions.show', compact('question', 'answers'));
    }

    public function create()
    {
        $categories = Category::all();

        return view('questions.buat', compact('categories'));
        // return view('questions.buat');
    }

    public function store(Request $request)
    {
        $request->validate([
            'category_id' => 'required',
            'title' => 'required',
            'content' => 'required',
            'image' => 'image|max:2048|mimes:png,jpg,jpeg'
        ]);

        $filename = null;
        if ($request->hasFile('image')) {
            $filename = time().'.'.$request->image->extension();
            $request->image->move(public_path('images'), $filename);
        }

        $category = Category::findOrFail($request->category_id);

        $question = new Question;
        $question->category_id = $category->id;
        $question->user_id = Auth::user()->id;
        $question->title = $request->title;
        $question->content = $request->content;
        $question->image = $filename;
        $question->save();

        return redirect('/questions')->with('success', 'Question created successfully.');
    }

    public function edit($id)
    {
        $question = Question::findOrFail($id);
        $categories = Category::all();

        return view('questions.edit', compact('question', 'categories'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'category_id' => 'required',
            'title' => 'required',
            'content' => 'required',
            'image' => 'image|max:2048|mimes:png,jpg,jpeg'
        ]);

        $question = Question::findOrFail($id);



        // Memastikan pengguna hanya dapat mengedit pertanyaan yang dimilikinya
        if ($question->user_id === Auth::user()->id) {
            $category = Category::findOrFail($request->category_id);

            if($request->has('image')){
                Storage::disk('public')->delete($question->image);
                $filename = time().'.'.$request->image->extension();
                $request->image->move(public_path('images'), $filename);

                $question->image = $filename;
                $question->save();
            }
            

            $question->category_id = $category->id;
            $question->title = $request->title;
            $question->content = $request->content;
            $question->save();

            return redirect('/questions')->with('success', 'Question updated successfully.');
        } else {
            // Jika pengguna mencoba mengedit pertanyaan milik orang lain, arahkan mereka ke halaman yang sesuai
            return redirect('/questions')->with('error', 'Anda tidak memiliki izin untuk mengedit pertanyaan ini.');
        }
    }

    public function destroy($id)
    {
        $question = Question::findOrFail($id);

        // Memastikan pengguna hanya dapat menghapus pertanyaan yang dimilikinya
        if ($question->user_id === Auth::user()->id) {
            $question->delete();

            Alert::success('Terhapus', 'Pertanyaan Anda berhasil dihapus!')->persistent(true)->autoClose(3000);
            return redirect('/questions')->with('success', 'Question deleted successfully.');
        } else {
            // Jika pengguna mencoba menghapus pertanyaan milik orang lain, arahkan mereka ke halaman yang sesuai
            Alert::error('Gagal!', 'Anda tidak bisa menghapus pertanyaan pengguna lain.');
            return redirect('/questions')->with('error', 'Anda tidak memiliki izin untuk menghapus pertanyaan ini.');
        }
    }
}
