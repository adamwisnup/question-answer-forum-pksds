@extends('layout.master')

@section('title')
    Form Ask Question
@endsection

@push('styles')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@push('script')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
  $(document).ready(function() {
    $('.js-example-basic-single').select2();
  });
</script>
<script src="https://cdn.tiny.cloud/1/85p5ol8qb2nod2ryfajrv37w9qj9rkcnqwsy07vnj75vskob/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
<script>
  tinymce.init({
    selector: 'textarea',
    plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount checklist mediaembed casechange export formatpainter pageembed linkchecker a11ychecker tinymcespellchecker permanentpen powerpaste advtable advcode editimage tinycomments tableofcontents footnotes mergetags autocorrect typography inlinecss',
    toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table mergetags | addcomment showcomments | spellcheckdialog a11ycheck typography | align lineheight | checklist numlist bullist indent outdent | emoticons charmap | removeformat',
    tinycomments_mode: 'embedded',
    tinycomments_author: 'Author name',
    mergetags_list: [
      { value: 'First.Name', title: 'First Name' },
      { value: 'Email', title: 'Email' },
    ]
  });
</script>
@endpush

@section('content')
  <section>
    <form action="/questions" method="POST" enctype="multipart/form-data">
      @csrf
      <div class="form-floating mb-3">
        <label for="floatingInputNama">Judul</label>
        <input type="text" class="form-control" id="floatingInputNama" placeholder="Tuliskan judul besar dari pertanyaan" name="title">
      </div>
      @error('title')
        <div class="alert alert-danger">{{$message}}</div>
      @enderror
      <div class="form-floating mb-3">
        <label for="floatingTextarea2">Apa detail masalah Anda?</label>
        <textarea class="form-control" placeholder="Tuliskan detail pertanyaan Anda dengan jelas" id="floatingTextarea2" style="height: 100px" name="content"></textarea>
      </div>
      @error('content')
        <div class="alert alert-danger">{{$message}}</div>
      @enderror
      <div class="form-floating mb-5">
        <label for="floatingInputKategori">Kategori</label>
        <select name="category_id" class="js-example-basic-single" style="width: 100%; height: 40px;">
          <option value="" disabled selected>Tambahkan kategori untuk menjelaskan tentang pertanyaan Anda</option>
          @forelse ($categories as $item )
          <option value="{{$item->id}}">{{$item->name}}</option>
          @empty
          <option value="">Kategori tidak ditemukan</option>
          @endforelse
        </select>
      </div>
      <div class="form-floating mb-5">
        <label for="floatingInputNama">Thumbnail</label>
        <input type="file" class="form-control-file" name="image" accept="image/*">
      </div>
      @error('image')
        <div class="alert alert-danger">{{$message}}</div>
      @enderror

      <button type="submit" class="btn btn-danger">Publish pertanyaan Saya</button>
    </form>
  </section>
@endsection 
